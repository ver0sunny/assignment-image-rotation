#include "../include/bmp.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>

struct bmp_header former_bmp_header(const struct image* img) {
    struct bmp_header bmp_header = {
            .bfType = 0x4D42,
            .bfileSize = img->height * img->width * sizeof(struct pixel) +
                         img->height * (img->width % 4) * sizeof(struct pixel),
                         //+ sizeof(BMPHEADER_STR),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage =  img->height * img->width * sizeof(struct pixel) + (img->width % 4) * img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    return bmp_header;
}

enum read_status from_bmp(FILE* in, struct image* img ) {
    struct bmp_header* bmp_header = {0};
    bmp_header = malloc(sizeof(struct bmp_header));
    //read the bitmap file header
    fread(bmp_header, sizeof(struct bmp_header), 1, in);
    //fread((void*)&bmp_header, 1, sizeof(BMPHEADER_STR), in);
    //verify that this is a bmp file by check bitmap id
    if (bmp_header->bfType !=0x4D42)
    {
        return READ_INVALID_HEADER;
    }

    //move file point to the beginning of bitmap data
    fseek(in, bmp_header->bOffBits, SEEK_SET);
    printf("%"PRIu32, bmp_header->biSize);
    printf("\n");
    img->height = bmp_header->biHeight;
    img->width = bmp_header->biWidth;
    img->data = malloc(img->height * img->width * sizeof(struct pixel));

    for (uint64_t i = 0; i < img->height; i++) {
        fread(&(img->data[i*img->width]), sizeof(struct pixel), img->width, in);
        fseek(in, (uint8_t)img->width%4, SEEK_CUR);
    }

    free(bmp_header);

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img ) {
    struct bmp_header bmp_header_former = former_bmp_header(img);

    fwrite(&bmp_header_former, sizeof(struct bmp_header), 1, out);

    const size_t fill = 0;
    for(uint64_t i=0; i< img->height; i++) {
        fwrite(&(img->data[i*img->width]), sizeof(struct pixel), img->width, out);
        fwrite(&fill, 1, img->width%4, out);
    }
    return WRITE_OK;
}

static char* const read_status_decoder[] = {
        [READ_OK] = "bmp read successfully \n",
        [READ_INVALID_HEADER] = "smth went wrong with reading \n"
};

static char* const write_status_decoder[] = {
        [WRITE_OK] = "bmp written successfully \n",
        [WRITE_ERROR] = "smth went wrong with writing \n"
};

void display_bmp_read_status(enum read_status status) {
    printf("%s", read_status_decoder[status]);
}

void display_bmp_write_status(enum write_status status) {
    printf("%s", write_status_decoder[status]);
}

