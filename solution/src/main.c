#include "../include/bmp.h"
#include "../include/file_io.h"
#include "../include/transform.h"

#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    struct image img;
    struct image transformed_img;

    const char* input_path = argv[1];
    FILE *file_in = {0}; //fopen("/Users/ver0sunny/CLionProjects/assignment-image-rotation/tester/tests/1/input.bmp", "rb"); //cюды функцию из file_io
//    display_file_read_status(read_file(&file_in, "/Users/ver0sunny/CLionProjects/assignment-image-rotation/tester/tests/1/input.bmp"));//argv[1])); //"/Users/ver0sunny/CLionProjects/assignment-image-rotation/tester/tests/3/input.bmp"));
    display_file_read_status(read_file(&file_in,input_path));

    display_bmp_read_status(from_bmp(file_in, &img));

    display_file_close_status(close_file(&file_in));

    transformed_img = rotate_90(&img);

    const char * output_path = argv[2];
    FILE *file_out = {0}; //fopen(argv[2], "wb"); //cюды функцию из file_io
//    display_file_write_status(write_file(&file_out, "/Users/ver0sunny/CLionProjects/assignment-image-rotation/tester/tests/1/new.bmp"));
    display_file_write_status(write_file(&file_out,output_path));

    display_bmp_write_status(to_bmp(file_out, &transformed_img));
    
    display_file_close_status(close_file(&file_out));

    free(img.data);
    free(transformed_img.data);

    return 0;
}
