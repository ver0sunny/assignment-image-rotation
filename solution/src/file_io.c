#include "../include/file_io.h"
#include <stdio.h>

enum file_read_status read_file(FILE** file, const char* path) {
    *file = fopen(path,"rb");
    if (path == NULL) return FILE_READ_PATH_NULL;
    if (*file == NULL) return FILE_READ_ERROR;
    else return FILE_READ_SUCCESS;
}

enum file_write_status write_file(FILE** file, const char* path) {
    *file = fopen(path, "wb");
    if (path == NULL) return FILE_WRITE_PATH_NULL;
    if (*file == NULL) return FILE_WRITE_ERROR;
    else return FILE_WRITE_SUCCESS;
}

enum file_close_status close_file(FILE** file) {
    if (*file == NULL) return FILE_CLOSE_ERROR;
    fclose(*file);
    return FILE_CLOSE_SUCCESS;
}

static const char* const read_status_decoder[] = {
        [FILE_READ_SUCCESS] = "file opened successfully \n",
        [FILE_READ_ERROR] = "smth went wrong with opening \n",
        [FILE_READ_PATH_NULL] = "you got no path, son \n"
};

static const char* const write_status_decoder[] = {
        [FILE_WRITE_SUCCESS] = "file opened for writing successfully \n",
        [FILE_WRITE_ERROR] = "smth went wrong with opening for writing \n",
        [FILE_WRITE_PATH_NULL] = "you got no path, son \n"
};

static const char* const close_status_decoder[] = {
        [FILE_CLOSE_SUCCESS] = "file closed successfully \n",
        [FILE_CLOSE_ERROR] = "smth went wrong with closing \n"
};

void display_file_read_status(FILE** log_file, enum file_read_status status) {
    fprintf(stderr, "%s", read_status_decoder[status]);
    fprintf(*log_file, "%s", read_status_decoder[status]);
}

void display_file_write_status(FILE** log_file, enum file_write_status status) {
    fprintf(stderr, "%s", write_status_decoder[status]);
    fprintf(*log_file, "%s", write_status_decoder[status]);
}

void display_file_close_status(FILE** log_file, enum file_close_status status) {
    fprintf(stderr, "%s", close_status_decoder[status]);
    fprintf(*log_file, "%s", close_status_decoder[status]);
}


