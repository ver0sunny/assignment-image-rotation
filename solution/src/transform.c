#include "../include/transform.h"
#include <stdio.h>
#include <stdlib.h>

void modify_copy_image(struct image* rotated, struct image const* source) {
    rotated->width = source->height;
    rotated->height = source->width;
    rotated->data = malloc(sizeof(struct pixel) * rotated->height * rotated->width);
}

struct pixel* get_rotated_img_pixel_adress(const struct image* img, size_t i, size_t j) {
    size_t index = i * img->width + j;
    return &img->data[index];
}

struct pixel get_source_img_pixel(const struct image* img, size_t i, size_t j) {
    size_t index = (img->height - 1 - j) * img->width + i;
    return img->data[index];
}

struct image rotate_90(struct image const* source ) {
    struct image rotated = {0};
    modify_copy_image(&rotated, source);
    for (size_t i = 0; i < source->width; ++i) {
        for (size_t j = 0; j < source->height; ++j) {
            *get_rotated_img_pixel_adress(&rotated,i,j) = get_source_img_pixel(source,i,j);
        }
    }
    return rotated;
}
