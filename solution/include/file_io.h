#ifndef FILE_IO_H
#define FILE_IO_H
#include <stdio.h>

enum file_read_status{
    FILE_READ_SUCCESS = 0,
    FILE_READ_ERROR,
    FILE_READ_PATH_NULL
};

enum file_write_status{
    FILE_WRITE_SUCCESS = 0,
    FILE_WRITE_ERROR,
    FILE_WRITE_PATH_NULL
};

enum file_close_status{
    FILE_CLOSE_SUCCESS = 0,
    FILE_CLOSE_ERROR
};

enum file_read_status read_file(FILE** file, const char* path);
enum file_write_status write_file(FILE** file, const char* path);
enum file_close_status close_file(FILE** file);

void display_file_read_status(FILE** log_file, enum file_read_status status);

void display_file_write_status(FILE** log_file, enum file_write_status status);

void display_file_close_status(FILE** log_file, enum file_close_status status);

#endif
