#ifndef TRANSFORM_H
#define TRANSFORM_H
#include "image.h"
#include <stddef.h>

void modify_copy_image(struct image* rotated, struct image const* source);

struct pixel* get_rotated_img_pixel_adress(const struct image* img, size_t i, size_t j);

struct pixel get_source_img_pixel(const struct image* img, size_t i, size_t j);

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate_90(struct image const* source );

#endif
